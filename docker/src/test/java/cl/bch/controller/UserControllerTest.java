package cl.bch.controller;

import static org.mockito.Matchers.any;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
//import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.internal.util.reflection.Whitebox;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import cl.bch.dto.SingleResponse;
import cl.bch.dto.UserDto;
import cl.bch.dto.UserName;
import cl.bch.enums.InternalCodes;
import cl.bch.exception.BchException;
import cl.bch.service.IUserService;
import cl.bch.util.GeneralUtils;

public class UserControllerTest {

	@Mock 
    IUserService userService;

    @Mock 
    GeneralUtils generalUtils;

    private UserController controller;

    private UserDto userDto;

    private UserName requestBody;

    private ObjectMapper mapper;

    private String authorization = "auth";

    private String consumer = "auth";

    private String trackid = "auth";

    private String apiKey = "auth";
	
	@BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        controller = new UserController();

        userDto = new UserDto();
        userDto.setId(Long.valueOf("1"));
        userDto.setName("Juan");
        userDto.setStatus(true);

        requestBody = new UserName();
        requestBody.setName(userDto.getName());
       
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    }

    @Test
    public void createUserOK() throws BchException {

        try {
            Mockito.when(userService.createUser(any(UserName.class))).thenReturn(userDto);
            //FieldSetter.setField(controller, UserController.class.getDeclaredField("userService"), userService);

            Whitebox.setInternalState(controller, "userService", userService);

            ResponseEntity<Object> response = (ResponseEntity<Object>)controller.createUser(authorization, consumer, trackid, apiKey, requestBody);
            SingleResponse singleResponse = new SingleResponse();
            singleResponse = mapper.convertValue(response.getBody(), SingleResponse.class);
            
            Assertions.assertEquals(response.getStatusCode().value(), HttpStatus.CREATED.value());
            Assertions.assertEquals(singleResponse.getItem().getId(), userDto.getId());
            Assertions.assertEquals(singleResponse.getItem().getName(), userDto.getName());
            Assertions.assertEquals(singleResponse.getItem().isStatus(), userDto.isStatus());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createDataAccessException() throws BchException, NoSuchFieldException, SecurityException {
        
        Mockito.doThrow(new BchException(InternalCodes.SQL_EXCEPTION.getReasonPhrase(), 
        InternalCodes.SQL_EXCEPTION.getHttpStatusCode(),InternalCodes.SQL_EXCEPTION.value()))
        .when(userService).createUser(any(UserName.class));
        //FieldSetter.setField(controller, UserController.class.getDeclaredField("userService"), userService);
        Whitebox.setInternalState(controller, "userService", userService);    
        ResponseEntity<Object> response = (ResponseEntity<Object>)controller.createUser(authorization, consumer, trackid, apiKey, requestBody);
        Assertions.assertEquals(response.getStatusCode().value(), InternalCodes.SQL_EXCEPTION.getHttpStatusCode());
    }
}