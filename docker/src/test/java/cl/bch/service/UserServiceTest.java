package cl.bch.service;

import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;

import cl.bch.dto.UserDto;
import cl.bch.dto.UserName;
import cl.bch.enums.InternalCodes;
import cl.bch.exception.BchException;
import cl.bch.pojo.User;
import cl.bch.repository.UserRepository;
import cl.bch.service.impl.UserServiceImpl;
import cl.bch.service.IUserService;

public class UserServiceTest {
	@Mock 
    private UserRepository userRepository;
    
    private IUserService userService;

    private User user;
    
    private UserDto userDto;

    private UserName userName;

    private List< User > lUser;

	@BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        userService = new UserServiceImpl();

        user = new User();
        user.setId(Long.valueOf("1"));
        user.setName("Juan");
        user.setStatus(true);

        User user2 = new User();
        user2.setId(Long.valueOf("2"));
        user2.setName("Pedro");
        user2.setStatus(true);

        userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setStatus(user.isStatus());

        userName = new UserName();
        userName.setName(userDto.getName());

        lUser = new ArrayList<>();
        lUser.add(user);
        lUser.add(user2);

    }

    @Test
    public void createUsuarioOK() throws BchException {
     
        Mockito.when(userRepository.save(any(User.class))).thenReturn(user);
        Whitebox.setInternalState(userService, "userRepository", userRepository);
        
        UserDto response = userService.createUser(userName);

        Assertions.assertEquals(response.getName(), userName.getName());
    }
 
    @Test
    public void getAllOK() throws BchException {
     
        Mockito.when(userRepository.findAll()).thenReturn(lUser);
        Whitebox.setInternalState(userService, "userRepository", userRepository);
        
        List<UserDto> response = userService.getAllUser();
        Assertions.assertEquals(response.size(), lUser.size());
        Assertions.assertEquals(response.get(0).getId(), lUser.get(0).getId());
        Assertions.assertEquals(response.get(0).getName(), lUser.get(0).getName());
        Assertions.assertEquals(response.get(1).getId(), lUser.get(1).getId());
        Assertions.assertEquals(response.get(1).getName(), lUser.get(1).getName());
    }

    @Test
    public void getByIdOK() throws BchException {
        Mockito.when(userRepository.getOne(any(Long.class))).thenReturn(user);
        Whitebox.setInternalState(userService, "userRepository", userRepository);
        
        UserDto response = userService.getUser(user.getId());
        
        Assertions.assertEquals(response.getId(), user.getId());
        Assertions.assertEquals(response.getName(), user.getName());
        Assertions.assertEquals(response.isStatus(), user.isStatus());
    }

    @Test
    public void updateOK() throws BchException {
        Mockito.when(userRepository.getOne(any(Long.class))).thenReturn(user);
        Mockito.when(userRepository.save(any(User.class))).thenReturn(user);
        Whitebox.setInternalState(userService, "userRepository", userRepository);

        userService.updateUser(userDto);
        
    }

    @Test
    public void deleteOK() throws BchException {
        Mockito.when(userRepository.getOne(any(Long.class))).thenReturn(user);
        user.setStatus(false); 
        Mockito.when(userRepository.save(any(User.class))).thenReturn(user);
        Whitebox.setInternalState(userService, "userRepository", userRepository);

        userService.deleteUser(user.getId());
        
    }

    @Test
    public void createException() throws BchException {
        
        Mockito.when(userRepository.save(any(User.class))).thenReturn(null);
        Whitebox.setInternalState(userService, "userRepository", userRepository);

        try{
            UserDto response = userService.createUser(userName);
        } catch (BchException e){
            Assertions.assertEquals(e.getMessage(),InternalCodes.USER_NOT_FOUND.getReasonPhrase());
        }
    }


}