package cl.bch.karate;

import com.intuit.karate.junit5.Karate;

class UserRunner {		
		@Karate.Test
	    Karate createUserTest() {
	        return new Karate().feature("createUser").relativeTo(getClass());
	    }
}
