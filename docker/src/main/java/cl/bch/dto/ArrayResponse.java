package cl.bch.dto;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * ArrayResponse
*
* @author Otto Ockrassa a.ockrassa.morales@accenture.com
*/

@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArrayResponse  extends ResourceSupport implements Serializable {
  static final long serialVersionUID = -3387516993124229948L;
  
  @JsonProperty("items")
  @Valid
  private List<UserDto> items = null;

  public ArrayResponse items(List<UserDto> items) {
    this.items = items;
    return this;
  }

  public ArrayResponse addItemsItem(UserDto itemsItem) {
    if (this.items == null) {
      this.items = new ArrayList<>();
    }
    this.items.add(itemsItem);
    return this;
  }

  /**
   * Get items
   * @return items
  **/

  @Valid
  public List<UserDto> getItems() {
    return items;
  }

  public void setItems(List<UserDto> items) {
    this.items = items;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ArrayResponse arrayResponse = (ArrayResponse) o;
    return Objects.equals(this.items, arrayResponse.items);
  }

  @Override
  public int hashCode() {
    return Objects.hash(items);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ArrayResponse {\n");
    
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

