package cl.bch.dto;

import java.io.Serializable;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.validation.annotation.Validated;

/**
 * EmptyResponse
 */
@Validated
public class EmptyResponse  extends ResourceSupport implements Serializable {


  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}