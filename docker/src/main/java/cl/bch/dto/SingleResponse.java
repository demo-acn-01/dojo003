package cl.bch.dto;

import java.io.Serializable;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonInclude;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;


/**
* @author Otto Ockrassa a.ockrassa.morales@accenture.com
*/

@JsonInclude(JsonInclude.Include.NON_NULL)
@Validated
public class SingleResponse extends ResourceSupport implements Serializable {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @JsonProperty("item")
  private UserDto item = null;

  public SingleResponse item(UserDto item) {
    this.item = item;
    return this;
  }

  /**
   * Get item
   * @return item
  **/
  @Valid

  public UserDto getItem() {
    return item;
  }

  public void setItem(UserDto item) {
    this.item = item;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SingleResponse singleResponse = (SingleResponse) o;
    return Objects.equals(this.item, singleResponse.item);
  }

  @Override
  public int hashCode() {
    return Objects.hash(item);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SingleResponse {\n");
    
    sb.append("    item: ").append(toIndentedString(item)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

