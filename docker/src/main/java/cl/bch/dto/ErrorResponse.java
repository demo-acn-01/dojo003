package cl.bch.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.hateoas.ResourceSupport;

/**
* Clase para respuestas de error.
*
*/

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse extends ResourceSupport implements Serializable{

	static final long serialVersionUID = -3387516993124229948L;
	
	private Integer internalCode;
	private String description;
	
	public Integer getInternalCode() {
		return internalCode;
	}
	public void setInternalCode(Integer internalCode) {
		this.internalCode = internalCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
