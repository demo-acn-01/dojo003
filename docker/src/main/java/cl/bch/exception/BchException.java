package cl.bch.exception;

/**
* Excepcion generica BCH.
*/

public class BchException extends Exception {

    static final long serialVersionUID = -3387516993124229948L;

    private int httpCode;
    private int internalCode;

    public BchException (String message, Throwable cause, int httpCode, int internalCode) {
        super(message,cause);
        this.httpCode = httpCode;
        this.internalCode = internalCode;
    }

    public BchException (String message, int httpCode, int internalCode) {
        super(message);
        this.httpCode = httpCode;
        this.internalCode = internalCode;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public int getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(int internalCode) {
        this.internalCode = internalCode;
    }
}
