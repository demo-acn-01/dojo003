package cl.bch.service;

import java.util.List;

import cl.bch.dto.UserDto;
import cl.bch.dto.UserName;
import cl.bch.exception.BchException;

/**
* Servicio de usuarios.
*
*/

public interface IUserService {
    UserDto createUser(UserName user) throws BchException;

    List<UserDto> getAllUser() throws BchException;

    UserDto getUser(Long id) throws BchException;

    void updateUser(UserDto user) throws BchException;

    void deleteUser(Long id) throws BchException;
}


