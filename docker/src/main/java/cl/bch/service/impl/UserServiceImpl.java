package cl.bch.service.impl;

import cl.bch.dto.UserDto;
import cl.bch.dto.UserName;
import cl.bch.enums.InternalCodes;
import cl.bch.exception.BchException;
import cl.bch.pojo.*;
import cl.bch.repository.UserRepository;
import cl.bch.service.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException.GatewayTimeout;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;
import org.springframework.web.client.HttpServerErrorException.ServiceUnavailable;

/**
* Implementacion de servicio de enrollment.
*
*/

@Service("UserService")
public class UserServiceImpl implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDto createUser(UserName user) throws BchException {
        try{
            User userObj = new User();
            userObj.setName(user.getName());
            userObj.setStatus(true);
            User response = userRepository.save(userObj);
            if ( response == null ) {
                throw new BchException(InternalCodes.USER_NOT_FOUND.getReasonPhrase(), 
                    InternalCodes.USER_NOT_FOUND.getHttpStatusCode(), InternalCodes.USER_NOT_FOUND.value());
            }
            UserDto resp = new UserDto();
            resp.setId(response.getId());
            resp.setName(response.getName());
            resp.setStatus(response.isStatus());
            return resp;
        }catch(ServiceUnavailable unavailable) {
            throw new BchException(InternalCodes.SERVICE_UNAVAILABLE.getReasonPhrase(), 
                InternalCodes.SERVICE_UNAVAILABLE.getHttpStatusCode(),InternalCodes.SERVICE_UNAVAILABLE.value());            
        } catch(GatewayTimeout timeOut) {
            throw new BchException(InternalCodes.GATEWAY_TIMEOUT.getReasonPhrase(), 
                InternalCodes.GATEWAY_TIMEOUT.getHttpStatusCode(),InternalCodes.GATEWAY_TIMEOUT.value());            
        }
        catch(InternalServerError serverError) {
            throw new BchException(InternalCodes.INTERNAL_SERVER_ERROR.getReasonPhrase(), 
                InternalCodes.INTERNAL_SERVER_ERROR.getHttpStatusCode(),InternalCodes.INTERNAL_SERVER_ERROR.value());            
        }
        catch(DataAccessException dae){
            throw new BchException(InternalCodes.SQL_EXCEPTION.getReasonPhrase(), InternalCodes.SQL_EXCEPTION.getHttpStatusCode(),
                InternalCodes.SQL_EXCEPTION.value());
        }
    }

    @Override
    public List<UserDto> getAllUser() throws BchException {
        try{
            List<User> response = userRepository.findAll();

            if ( response.isEmpty() || response == null){
                throw new BchException(InternalCodes.USER_NOT_FOUND.getReasonPhrase(), 
                    InternalCodes.USER_NOT_FOUND.getHttpStatusCode(), InternalCodes.USER_NOT_FOUND.value());
            }

            List<UserDto> resp = new ArrayList<>();
            for( User user : response ){
                UserDto userDto = new  UserDto();
                userDto.setId(user.getId());
                userDto.setName(user.getName());
                userDto.setStatus(user.isStatus());
                resp.add(userDto);
            }
           
            return resp;
        }catch(ServiceUnavailable unavailable) {
            throw new BchException(InternalCodes.SERVICE_UNAVAILABLE.getReasonPhrase(), 
                InternalCodes.SERVICE_UNAVAILABLE.getHttpStatusCode(),InternalCodes.SERVICE_UNAVAILABLE.value());            
        } catch(GatewayTimeout timeOut) {
            throw new BchException(InternalCodes.GATEWAY_TIMEOUT.getReasonPhrase(), 
                InternalCodes.GATEWAY_TIMEOUT.getHttpStatusCode(),InternalCodes.GATEWAY_TIMEOUT.value());            
        }
        catch(InternalServerError serverError) {
            throw new BchException(InternalCodes.INTERNAL_SERVER_ERROR.getReasonPhrase(), 
                InternalCodes.INTERNAL_SERVER_ERROR.getHttpStatusCode(),InternalCodes.INTERNAL_SERVER_ERROR.value());            
        }
        catch(DataAccessException dae){
            throw new BchException(InternalCodes.SQL_EXCEPTION.getReasonPhrase(), InternalCodes.SQL_EXCEPTION.getHttpStatusCode(),
                InternalCodes.SQL_EXCEPTION.value());
        }
    }

    @Override
    public UserDto getUser(Long id) throws BchException {
        try{
            User response = userRepository.getOne(id);
            if (response.getName() == null){
                throw new BchException(InternalCodes.USER_NOT_FOUND.getReasonPhrase(), 
                    InternalCodes.USER_NOT_FOUND.getHttpStatusCode(), InternalCodes.USER_NOT_FOUND.value());
            }
            UserDto resp = new UserDto();
            resp.setId(response.getId());
            resp.setName(response.getName());
            resp.setStatus(response.isStatus());
            return resp;
        }catch(ServiceUnavailable unavailable) {
            throw new BchException(InternalCodes.SERVICE_UNAVAILABLE.getReasonPhrase(), 
                InternalCodes.SERVICE_UNAVAILABLE.getHttpStatusCode(),InternalCodes.SERVICE_UNAVAILABLE.value());            
        } catch(GatewayTimeout timeOut) {
            throw new BchException(InternalCodes.GATEWAY_TIMEOUT.getReasonPhrase(), 
                InternalCodes.GATEWAY_TIMEOUT.getHttpStatusCode(),InternalCodes.GATEWAY_TIMEOUT.value());            
        }
        catch(InternalServerError serverError) {
            throw new BchException(InternalCodes.INTERNAL_SERVER_ERROR.getReasonPhrase(), 
                InternalCodes.INTERNAL_SERVER_ERROR.getHttpStatusCode(),InternalCodes.INTERNAL_SERVER_ERROR.value());            
        }
        catch(DataAccessException dae){
            throw new BchException(InternalCodes.SQL_EXCEPTION.getReasonPhrase(), InternalCodes.SQL_EXCEPTION.getHttpStatusCode(),
                InternalCodes.SQL_EXCEPTION.value());
        }
        catch(EntityNotFoundException entityNotDound){
            throw new BchException(InternalCodes.USER_NOT_FOUND.getReasonPhrase(), 
            InternalCodes.USER_NOT_FOUND.getHttpStatusCode(), InternalCodes.USER_NOT_FOUND.value());
        }
    }

    @Override
    public void updateUser(UserDto user) throws BchException {
        try{
            User userId = userRepository.getOne(user.getId());
            userId.setName(user.getName());
            userId.setStatus(user.isStatus());
            User response =  userRepository.save(userId);
            if(response == null ) {
                throw new BchException(InternalCodes.USER_NOT_FOUND.getReasonPhrase(), 
                    InternalCodes.USER_NOT_FOUND.getHttpStatusCode(), InternalCodes.USER_NOT_FOUND.value());
            }
            UserDto resp = new UserDto();
            resp.setId(response.getId());
            resp.setName(response.getName());
            resp.setStatus(response.isStatus());
        }catch(ServiceUnavailable unavailable) {
            throw new BchException(InternalCodes.SERVICE_UNAVAILABLE.getReasonPhrase(), 
                InternalCodes.SERVICE_UNAVAILABLE.getHttpStatusCode(),InternalCodes.SERVICE_UNAVAILABLE.value());            
        } catch(GatewayTimeout timeOut) {
            throw new BchException(InternalCodes.GATEWAY_TIMEOUT.getReasonPhrase(), 
                InternalCodes.GATEWAY_TIMEOUT.getHttpStatusCode(),InternalCodes.GATEWAY_TIMEOUT.value());            
        }
        catch(InternalServerError serverError) {
            throw new BchException(InternalCodes.INTERNAL_SERVER_ERROR.getReasonPhrase(), 
                InternalCodes.INTERNAL_SERVER_ERROR.getHttpStatusCode(),InternalCodes.INTERNAL_SERVER_ERROR.value());            
        }
        catch(DataAccessException dae){
            throw new BchException(InternalCodes.SQL_EXCEPTION.getReasonPhrase(), InternalCodes.SQL_EXCEPTION.getHttpStatusCode(),
                InternalCodes.SQL_EXCEPTION.value());
        }       
    }

    @Override
    public void deleteUser(Long id) throws BchException {
        try{
            User user = userRepository.getOne(id);
            if(user != null){
                userRepository.delete(user);
            }
            else{
                throw new BchException(InternalCodes.USER_NOT_FOUND.getReasonPhrase(), 
                    InternalCodes.USER_NOT_FOUND.getHttpStatusCode(), InternalCodes.USER_NOT_FOUND.value());
            }
        }catch(ServiceUnavailable unavailable) {
            throw new BchException(InternalCodes.SERVICE_UNAVAILABLE.getReasonPhrase(), 
                InternalCodes.SERVICE_UNAVAILABLE.getHttpStatusCode(),InternalCodes.SERVICE_UNAVAILABLE.value());            
        } catch(GatewayTimeout timeOut) {
            throw new BchException(InternalCodes.GATEWAY_TIMEOUT.getReasonPhrase(), 
                InternalCodes.GATEWAY_TIMEOUT.getHttpStatusCode(),InternalCodes.GATEWAY_TIMEOUT.value());            
        }
        catch(InternalServerError serverError) {
            throw new BchException(InternalCodes.INTERNAL_SERVER_ERROR.getReasonPhrase(), 
                InternalCodes.INTERNAL_SERVER_ERROR.getHttpStatusCode(),InternalCodes.INTERNAL_SERVER_ERROR.value());            
        }
        catch(DataAccessException dae){
            throw new BchException(InternalCodes.SQL_EXCEPTION.getReasonPhrase(), InternalCodes.SQL_EXCEPTION.getHttpStatusCode(),
                InternalCodes.SQL_EXCEPTION.value());
        }
    }


}
