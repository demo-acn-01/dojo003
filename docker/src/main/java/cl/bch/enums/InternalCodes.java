package cl.bch.enums;

/**
 * Enumerado de codigos de error internos
 *
 * @since 1.0
 * @version 1.0
 */

public enum InternalCodes {

	REQUIRED_DATA_EMPTY(1, "Campos obligatorios vacios", 400),
	REQUIRED_HEADERS_EMPTY(2, "Headers obligatorios vacios", 400),
	REQUIRED_ID(2, "ID vacio o nulo", 400),
    USER_NOT_AUTHORIZED(3, "Usuario no autorizado", 401),
    FORBIDDEN(4, "No tiene permisos para acceder a la informacion", 403),
    USER_NOT_FOUND(5, "No se encuentran datos", 404),
    INTERNAL_SERVER_ERROR(4, "Acceso invalido al recurso", 500),
    SERVICE_UNAVAILABLE(5, "Servicio no disponible", 503),
	GATEWAY_TIMEOUT(6, "Tiempo de respuesta mayor a lo esperado", 504),
	SQL_EXCEPTION(7, "Error en motor de base de datos", 404);

	private final int value;

	private final String reasonPhrase;

	private final int httpStatusCode;

	/**
	 * 
	 * @param value
	 * @param reasonPhrase
	 * @param httpStatusCode
	 */
	InternalCodes(int value, String reasonPhrase, int httpStatusCode) {
		this.value = value;
		this.reasonPhrase = reasonPhrase;
		this.httpStatusCode = httpStatusCode;
	}

	/**
	 * 
	 * @return int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * 
	 * @return String
	 */
	public String getReasonPhrase() {
		return this.reasonPhrase;
	}

	/**
	 * @return string
	 */
	public String toString() {
		return this.value + " " + name();
	}

	/**
	 * 
	 * @return int
	 */
	public int getHttpStatusCode() {
		return httpStatusCode;
	}

}