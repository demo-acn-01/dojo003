
package cl.bch.pojo;

/**
* Pojo que describe el obejeto User.
*
*/

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "user_CL")
public class User {

	@Id
	@GeneratedValue(
		strategy= GenerationType.AUTO,
		generator="native"
	)
	@GenericGenerator(
		name = "native",
		strategy = "native"
	)
    private Long id;
    private String name;
    private Boolean status;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
    }
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
    }
    
    public Boolean isStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
    }

}