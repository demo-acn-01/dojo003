package cl.bch.controller;

import cl.bch.service.*;
import cl.bch.dto.ArrayResponse;
import cl.bch.dto.EmptyResponse;
import cl.bch.dto.ErrorResponse;
import cl.bch.dto.SingleResponse;
import cl.bch.dto.UserDto;
import cl.bch.dto.UserName;
import cl.bch.exception.BchException;
import cl.bch.util.GeneralUtils;
import cl.bch.util.HeadersUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RequestBody;

/**
* Controlador del servicio de User.
*
* @author d.gajardo.iglesias@accenture.com
*/

@RestController
@RequestMapping("v1")
public class UserController {
	/**
     *
     */
	static final Logger logger = LoggerFactory.getLogger(UserController.class);
	static final String JAVA_PATH = "/users";
	static final String JAVA_PATH_ID = JAVA_PATH +"/id";

    HeadersUtils header = new HeadersUtils();

	GeneralUtils generalUtils = new GeneralUtils();
	
	@Autowired
	@Qualifier("UserService")
	IUserService userService;

	/**
     * Crea un objeto en la bd con los datos ingresados
     *
     * @param authorization  Contiene el token generado por los datos de login 'user:codigo'' donde codigo es el codigo a validar. Encodeado en Base64
     * @param consumer Identificador del canal o producto que consume el servicio.
	 * @param trackid Id de seguimiento único para seguimiento de traza.
	 * @param apiKey Token de acceso de la aplicación.
	 * @param requestBody Body de la peticion
     * @return Retorna el objeto creado y los links asociados
     */

	@Operation(summary ="Operación crear usuario")
    @ApiResponses({
        @ApiResponse(responseCode ="201",description ="Creado", content = @Content( mediaType = "", schema = @Schema(implementation =  SingleResponse.class))),
        @ApiResponse(responseCode="400",description = "Headers obligatorios vacios", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="403",description = "No tiene permisos para acceder a la informacion", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="404",description = "Error en motor de base de datos", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="500",description = "Acceso invalido al recurso", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="503",description = "Servicio no disponible", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="504",description = "Tiempo de respuesta mayor a lo esperado", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class)))
	})
	@RequestMapping(value = JAVA_PATH, method=RequestMethod.POST)
	public ResponseEntity<Object> createUser( @RequestHeader("Authorization") String authorization , @RequestHeader("X-Consumer") String consumer, @RequestHeader("X-TrackId") String trackid, @RequestHeader("X-api-key") String apiKey, @RequestBody(required=false) UserName requestBody) {
		logger.info("createUser - init");
		try {
			SingleResponse singleResponse = new SingleResponse();

			generalUtils.validateHeaders(authorization, consumer, trackid, apiKey);
			generalUtils.validateUserName(requestBody);
			generalUtils.linkSingleResponse(singleResponse, JAVA_PATH, JAVA_PATH_ID, UserController.class);

			UserDto response = this.userService.createUser(requestBody);
			singleResponse.setItem(response);
			logger.info("createUser - end");
            return new ResponseEntity<>(singleResponse, header.getGenericHeaders(), HttpStatus.CREATED);
			
		} catch (BchException e) {
			ErrorResponse errorResponse = new ErrorResponse();
			
			generalUtils.linkErrorResponse(errorResponse, JAVA_PATH, JAVA_PATH_ID, UserController.class);

			errorResponse.setInternalCode(e.getInternalCode());
			errorResponse.setDescription(e.getMessage());
			logger.error("createUser - error :" + errorResponse);
	      	return new ResponseEntity<>(
	            errorResponse,
	            header.getGenericHeaders() ,
	            HttpStatus.valueOf(e.getHttpCode()));
		}
	}

	/**
     * Actualiza un objeto en la bd con los datos ingresados
     *
     * @param authorization  Contiene el token generado por los datos de login 'user:codigo'' donde codigo es el codigo a validar. Encodeado en Base64
     * @param consumer Identificador del canal o producto que consume el servicio.
	 * @param trackid Id de seguimiento único para seguimiento de traza.
	 * @param apiKey Token de acceso de la aplicación.
	 * @param requestBody Body de la peticion
     * @return Retorna 201 y los links asociados al recurso
     */

	@Operation(summary ="Operación actualizar usuario")
    @ApiResponses({
        @ApiResponse(responseCode ="201",description ="Creado", content = @Content( mediaType = "", schema = @Schema(implementation =  EmptyResponse.class))),
        @ApiResponse(responseCode="400",description = "Headers obligatorios vacios", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="403",description = "No tiene permisos para acceder a la informacion", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="404",description = "Error en motor de base de datos", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="500",description = "Acceso invalido al recurso", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="503",description = "Servicio no disponible", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="504",description = "Tiempo de respuesta mayor a lo esperado", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class)))
	})
	@RequestMapping(value = JAVA_PATH, method=RequestMethod.PUT)
	public ResponseEntity<Object> updateUser ( @RequestHeader("Authorization") String authorization , @RequestHeader("X-Consumer") String consumer, @RequestHeader("X-TrackId") String trackid, @RequestHeader("X-api-key") String apiKey, @RequestBody(required=false) UserDto requestBody) {
		logger.info("updateUser - init");
		try {
			EmptyResponse emptyResponse = new EmptyResponse();

			generalUtils.validateHeaders( authorization, consumer, trackid, apiKey);
			generalUtils.validateUser(requestBody);
			generalUtils.linkEmptyResponse(emptyResponse, JAVA_PATH, JAVA_PATH_ID, UserController.class);

			this.userService.updateUser(requestBody);
			logger.info("updateUser - end");
            return new ResponseEntity<>(emptyResponse, header.getGenericHeaders(), HttpStatus.CREATED);
			
		} catch (BchException e) {
			ErrorResponse errorResponse = new ErrorResponse();
			generalUtils.linkErrorResponse(errorResponse, JAVA_PATH, JAVA_PATH_ID, UserController.class);
			errorResponse.setInternalCode(e.getInternalCode());
			errorResponse.setDescription(e.getMessage());

			logger.error("updateUser - error :" + errorResponse);
	      	return new ResponseEntity<>(
	            errorResponse,
	            header.getGenericHeaders() ,
	            HttpStatus.valueOf(e.getHttpCode()));
		}
	}

	/**
     * Actualiza un objeto en la bd con los datos ingresados
     *
     * @param authorization  Contiene el token generado por los datos de login 'user:codigo'' donde codigo es el codigo a validar. Encodeado en Base64
     * @param consumer Identificador del canal o producto que consume el servicio.
	 * @param trackid Id de seguimiento único para seguimiento de traza.
	 * @param apiKey Token de acceso de la aplicación.
     * @return Retorna un array de los usuarios de la app y los links asociados al recurso
     */

	@Operation(summary ="Operación obtener todos los usuarios")
    @ApiResponses({
        @ApiResponse(responseCode ="200",description ="Ok", content = @Content( mediaType = "", schema = @Schema(implementation =  ArrayResponse.class))),
        @ApiResponse(responseCode="400",description = "Headers obligatorios vacios", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="403",description = "No tiene permisos para acceder a la informacion", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="404",description = "Error en motor de base de datos", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="500",description = "Acceso invalido al recurso", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="503",description = "Servicio no disponible", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="504",description = "Tiempo de respuesta mayor a lo esperado", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class)))
	})
	@RequestMapping(value = JAVA_PATH, method=RequestMethod.GET)
	public ResponseEntity<Object> getAllUser ( @RequestHeader("Authorization") String authorization, @RequestHeader("X-Consumer") String consumer, @RequestHeader("X-TrackId") String trackid, @RequestHeader("X-api-key") String apiKey) {
		logger.info("getAllUser - init");
		try {
			ArrayResponse arrayResponse = new ArrayResponse();

			generalUtils.validateHeaders( authorization, consumer, trackid, apiKey);
			generalUtils.linkArrayResponse(arrayResponse, JAVA_PATH, JAVA_PATH_ID, UserController.class);
	
			List<UserDto> response = this.userService.getAllUser();
			arrayResponse.items(response);

			logger.info("getAllUser - end");
			return  new ResponseEntity<>(arrayResponse, header.getGenericHeaders(), HttpStatus.OK);
		} catch (BchException e) {
			ErrorResponse errorResponse = new ErrorResponse();
			generalUtils.linkErrorResponse(errorResponse, JAVA_PATH, JAVA_PATH_ID, UserController.class);
			errorResponse.setInternalCode(e.getInternalCode());
			errorResponse.setDescription(e.getMessage());

			logger.error("getAllUser - error :" + errorResponse);
	        return new ResponseEntity<>(
	            errorResponse,
	            header.getGenericHeaders() ,
	            HttpStatus.valueOf(e.getHttpCode()));
		}
	}

	/**
     * busca un objeto en la bd segun el id
     *
     * @param authorization  Contiene el token generado por los datos de login 'user:codigo'' donde codigo es el codigo a validar. Encodeado en Base64
     * @param consumer Identificador del canal o producto que consume el servicio.
	 * @param trackid Id de seguimiento único para seguimiento de traza.
	 * @param apiKey Token de acceso de la aplicación.
	 * @param id Es el id del objeto a buscar
     * @return Retorna el objeto buscado y los links asociados al recurso
     */

	@Operation(summary ="Operación obtener usuario segun Id")
    @ApiResponses({
        @ApiResponse(responseCode ="200",description ="Ok", content = @Content( mediaType = "", schema = @Schema(implementation =  SingleResponse.class))),
        @ApiResponse(responseCode="400",description = "Headers obligatorios vacios", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="403",description = "No tiene permisos para acceder a la informacion", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="404",description = "Error en motor de base de datos", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="500",description = "Acceso invalido al recurso", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="503",description = "Servicio no disponible", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="504",description = "Tiempo de respuesta mayor a lo esperado", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class)))
	})	 
	@RequestMapping(value = JAVA_PATH + "/{id}", method=RequestMethod.GET)
	public ResponseEntity<Object> getUserById (@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization , @RequestHeader("X-Consumer") String consumer, @RequestHeader("X-TrackId") String trackid, @RequestHeader("X-api-key") String apiKey) {
		logger.info("getUserById - init");		
		try {
			SingleResponse singleResponse = new SingleResponse();

			generalUtils.validateHeaders( authorization, consumer, trackid, apiKey);
			generalUtils.validateId(id);
			generalUtils.linkSingleResponse(singleResponse, JAVA_PATH, JAVA_PATH_ID, UserController.class);
	
			UserDto response = this.userService.getUser(id);
			singleResponse.item(response);

			logger.info("getUserById - end");
			return new ResponseEntity<>(singleResponse, header.getGenericHeaders(), HttpStatus.OK);
		} catch (BchException e) {
			ErrorResponse errorResponse = new ErrorResponse();
			generalUtils.linkErrorResponse(errorResponse, JAVA_PATH, JAVA_PATH_ID, UserController.class);
			errorResponse.setInternalCode(e.getInternalCode());
			errorResponse.setDescription(e.getMessage());

			logger.error("getUserById - error :" + errorResponse);
	      	return new ResponseEntity<>(
	            errorResponse,
	            header.getGenericHeaders() ,
	            HttpStatus.valueOf(e.getHttpCode()));
		}
	}

	/**
     * elimina un objeto en la bd segun el id
     *
     * @param authorization  Contiene el token generado por los datos de login 'user:codigo'' donde codigo es el codigo a validar. Encodeado en Base64
     * @param consumer Identificador del canal o producto que consume el servicio.
	 * @param trackid Id de seguimiento único para seguimiento de traza.
	 * @param apiKey Token de acceso de la aplicación.
	 * @param id Es el id del objeto a buscar
     * @return Retorna un codigo 200 y los links asociados al recurso
     */
	@Operation(summary ="Operación que elimina usuario segun Id")
    @ApiResponses({
        @ApiResponse(responseCode ="200",description ="Ok", content = @Content( mediaType = "", schema = @Schema(implementation =  EmptyResponse.class))),
        @ApiResponse(responseCode="400",description = "Headers obligatorios vacios", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="403",description = "No tiene permisos para acceder a la informacion", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="404",description = "Error en motor de base de datos", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="500",description = "Acceso invalido al recurso", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="503",description = "Servicio no disponible", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class))),
		@ApiResponse(responseCode="504",description = "Tiempo de respuesta mayor a lo esperado", content = @Content( mediaType = "", schema = @Schema(implementation =  ErrorResponse.class)))
	})	 
	@RequestMapping(value = JAVA_PATH  + "/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Object> deleteUser (@PathVariable("id") Long id, @RequestHeader("Authorization") String authorization , @RequestHeader("X-Consumer") String consumer, @RequestHeader("X-TrackId") String trackid, @RequestHeader("X-api-key") String apiKey) {	
		logger.info("deleteUser - init");
		try {
			EmptyResponse emptyResponse = new EmptyResponse();

			generalUtils.validateHeaders( authorization, consumer, trackid, apiKey);
			generalUtils.validateId(id);
			generalUtils.linkEmptyResponse(emptyResponse, JAVA_PATH, JAVA_PATH_ID, UserController.class);
			this.userService.deleteUser(id);

			logger.info("deleteUser - end");
			return new ResponseEntity<>(emptyResponse, header.getGenericHeaders(), HttpStatus.OK);
		} catch (BchException e) {
			ErrorResponse errorResponse = new ErrorResponse();
			generalUtils.linkErrorResponse(errorResponse, JAVA_PATH, JAVA_PATH_ID, UserController.class);
			errorResponse.setInternalCode(e.getInternalCode());
			errorResponse.setDescription(e.getMessage());

			logger.error("deleteUser - error :" + errorResponse);
	      	return new ResponseEntity<>(
	            errorResponse,
	            header.getGenericHeaders() ,
	            HttpStatus.valueOf(e.getHttpCode()));
		}
	}
}
