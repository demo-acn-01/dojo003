package cl.bch.util;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;

import cl.bch.dto.ArrayResponse;
import cl.bch.dto.EmptyResponse;
import cl.bch.dto.ErrorResponse;
import cl.bch.dto.SingleResponse;
import cl.bch.dto.UserDto;
import cl.bch.dto.UserName;
import cl.bch.enums.InternalCodes;
import cl.bch.exception.BchException;


/**
* Headers de respuesta genericos.
*
*/

public class GeneralUtils {

	public void validateHeaders ( String authorization,  String consumer,  String trackid,  String apiKey ) throws BchException {
		if ( authorization.isEmpty() || consumer.isEmpty() ||
		 	trackid.isEmpty() || apiKey.isEmpty() ) {
			throw new BchException(InternalCodes.REQUIRED_HEADERS_EMPTY.getReasonPhrase(), 
				InternalCodes.REQUIRED_HEADERS_EMPTY.getHttpStatusCode(), InternalCodes.REQUIRED_HEADERS_EMPTY.value());
		}
	}
	
	public void validateUserName (UserName userName) throws BchException {

		if ( userName == null || userName.getName().replaceAll("\\s","").isEmpty() ) {
			throw new BchException(InternalCodes.REQUIRED_HEADERS_EMPTY.getReasonPhrase(), 
				InternalCodes.REQUIRED_DATA_EMPTY.getHttpStatusCode(), InternalCodes.REQUIRED_DATA_EMPTY.value());
		}
	}
	


	public void validateUser (UserDto userDto) throws BchException {

		if ( userDto == null || userDto.getName().replaceAll("\\s","").isEmpty() ) {
			throw new BchException(InternalCodes.REQUIRED_HEADERS_EMPTY.getReasonPhrase(), 
				InternalCodes.REQUIRED_DATA_EMPTY.getHttpStatusCode(), InternalCodes.REQUIRED_DATA_EMPTY.value());
		}
	}

	public void validateTwoId (Long id, Long id2) throws BchException {

		if ( (id == null || id < 0 ) && (id2 == null || id2 < 0 )) {
			throw new BchException(InternalCodes.REQUIRED_ID.getReasonPhrase(), 
				InternalCodes.REQUIRED_ID.getHttpStatusCode(), InternalCodes.REQUIRED_ID.value());
		}
	}

	public void validateId (Long id) throws BchException {

		if ( id == null || id < 0 ) {
			throw new BchException(InternalCodes.REQUIRED_ID.getReasonPhrase(), 
				InternalCodes.REQUIRED_ID.getHttpStatusCode(), InternalCodes.REQUIRED_ID.value());
		}
	}

	public SingleResponse linkSingleResponse ( SingleResponse singleResponse,  String javaPath, String javaPathId, Class nameClass) {
		Link selfLink = ControllerLinkBuilder
                .linkTo(nameClass)
                .slash(javaPath)
				.withRel("get");
			
				singleResponse.add(selfLink);

			selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPathId)
			.withRel("get");
			
			singleResponse.add(selfLink);
            
            selfLink = ControllerLinkBuilder
                .linkTo(nameClass)
                .slash(javaPath)
				.withRel("post");
			
			singleResponse.add(selfLink);

			selfLink = ControllerLinkBuilder
                .linkTo(nameClass)
                .slash(javaPath)
				.withRel("put");
			
			singleResponse.add(selfLink);

			selfLink = ControllerLinkBuilder
                .linkTo(nameClass)
                .slash(javaPathId)
				.withRel("delete");
			
			singleResponse.add(selfLink);
			return singleResponse;
	} 

	public ErrorResponse linkErrorResponse ( ErrorResponse errorResponse,  String javaPath, String javaPathId, Class nameClass) {
		Link selfLink = ControllerLinkBuilder
                .linkTo(nameClass)
                .slash(javaPath)
				.withRel("get");
			
				errorResponse.add(selfLink);

		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPathId)
			.withRel("get");
		
		errorResponse.add(selfLink);
		
		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPath)
			.withRel("post");
		
		errorResponse.add(selfLink);

		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPath)
			.withRel("put");
		
		errorResponse.add(selfLink);

		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPathId)
			.withRel("delete");
		
		errorResponse.add(selfLink);

		return errorResponse;
	} 

	public ArrayResponse linkArrayResponse ( ArrayResponse arrayResponse,  String javaPath, String javaPathId, Class nameClass) {
		Link selfLink = ControllerLinkBuilder
                .linkTo(nameClass)
                .slash(javaPath)
				.withRel("get");
			
		arrayResponse.add(selfLink);

		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPathId)
			.withRel("get");
			
		arrayResponse.add(selfLink);
		
		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPath)
			.withRel("post");
			
		arrayResponse.add(selfLink);

		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPath)
			.withRel("put");
		
			arrayResponse.add(selfLink);

		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPathId)
			.withRel("delete");
		
		arrayResponse.add(selfLink);

		return arrayResponse;
	} 

    public EmptyResponse linkEmptyResponse ( EmptyResponse emptyResponse, String javaPath, String javaPathId, Class nameClass) {
		Link selfLink = ControllerLinkBuilder
                .linkTo(nameClass)
                .slash(javaPath)
				.withRel("get");
			
		emptyResponse.add(selfLink);

		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPathId)
			.withRel("get");
			
		emptyResponse.add(selfLink);
		
		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPath)
			.withRel("post");
			
		emptyResponse.add(selfLink);

		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPath)
			.withRel("put");
		
		emptyResponse.add(selfLink);

		selfLink = ControllerLinkBuilder
			.linkTo(nameClass)
			.slash(javaPathId)
			.withRel("delete");
		
		emptyResponse.add(selfLink);

		return emptyResponse;
	} 
}
