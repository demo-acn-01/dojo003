package cl.bch.util;

import org.springframework.http.HttpHeaders;

/**
* Headers de respuesta genericos.
*
* @author Mariano Gonzalez mariano.accenture@accenture.com
*/

public class HeadersUtils {
	
	public HttpHeaders getGenericHeaders() {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Access-Control-Allow-Methods", "*");
		headers.add("Access-Control-Max-Age", "3600");
	    headers.add("Access-Control-Allow-Credentials", "false");
		headers.add("Access-Control-Allow-Headers", "*");  
		headers.add("X-Frame-Options", "deny");  
		headers.add("X-XSS-Protection", "1; mode=block");  
		headers.add("Strict-Transport-Security", "max-age=31536000; includeSubDomains");  
		return headers;
	}

}
