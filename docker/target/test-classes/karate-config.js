function fn() {   
  var host = karate.properties['karate.host'];// get java system property 'karate.host'
  var port = karate.properties['karate.port'];// get java system property 'karate.port'
  
  karate.log('karate.host system property was:', host);
  karate.log('karate.host system property was:', port);

  var config = { // base config JSON
    appId: 'my.app.id',
    appSecret: 'my.secret',
    urlBase: 'http://' +host+ ':' +port+ '/msuser/v1',
  };
  // don't waste time waiting for a connection or if servers don't respond within 5 seconds
  karate.configure('connectTimeout', 5000);
  karate.configure('readTimeout', 5000);
  return config;
}