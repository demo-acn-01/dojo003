Feature: createUser

  Background:
  * url urlBase

  Scenario: create user

  Given path '/users'
  And def requestBody =
    """
    {
      "name": "pepe",
    }
    """
  And request requestBody
  And header Authorization = 'auth'
  And header X-Consumer = 'auth'
  And header X-TrackId = 'auth'
  And header X-api-key = 'auth'
  When method post
  Then status 201

  Scenario: create user with no header

  Given path '/users'
  And def requestBody =
    """
    {
      "name": "pepe",
    }
    """
  And request requestBody
  And header X-Consumer = 'auth'
  And header X-TrackId = 'auth'
  When method post
  Then status 400

