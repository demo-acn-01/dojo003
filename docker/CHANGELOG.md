# Changelog
Todos los cambios notables a este proyecto se documentarán en este archivo.
El formato se de este *changelog* basa en [Keep a Changelog](http://keepachangelog.com/) y este proyecto se adhiere a [Semantic Versioning](http://semver.org/).

## Unreleased
- Introduzca aquí el listado de futuras incorporaciones para el proyecto.

## [1.0.0] - Introduzca aquí la fecha de release
### Added
- Introduzca aquí el listado de incorporaciones para la versión correspondiente.

### Changed
- Introduzca aquí el listado de cambios para la versión correspondiente.

### Removed
- Introduzca aquí el listado de eliminaciones para la versión correspondiente.