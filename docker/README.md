# Projecto

[Documentar aquí la descripción del proyecto que hace uso del arquetipo.]

## Ownership

| Rol               |                                                         |
|---                |---                                                      |
| Arquitecto        |  [Indique aquí el nombre del arquitecto responsable.]   |
| Líder técnico     |  [Indique aquí el nombre del líder técnico.]            |
| Product Owner     |  [Indique aquí el nombre del product owner, si aplica.] |

# Dependencias y requerimientos

[Indique aquí las dependencias y requerimientos existentes para la ejecución del proyecto.]

# Modo de uso

[Indique aquí de forma detallada el modo de uso del presente desarrollo. Considere aspectos de instalación y ejecución/consumo. Incorpore imagenes de ser necesario.]